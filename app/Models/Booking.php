<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;

    protected $dates=['deleted_at'];
    protected $fillable=[
        'name',
        'phoneNumber',
        'address',
        'note',
        'file_name',
        'file_path',
        'file_type',
    ];
}
