<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    use HasFactory;
    protected $dates=['deleted_at'];
    protected $fillable=[
        'nameOfArabic',
        'nameOfEnglish',
        'gender',
        'dob',
        'nationalNumber',
        'nationality',
        'IdPatient',
        'passwordPatient',
        'testName',
        'testResult',
        'collectionDate',
        'reportingDate',
        'Unit',
        'brunch',
        'range',
        'report',
        'imagePatient',
    ];
}

