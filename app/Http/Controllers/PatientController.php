<?php

namespace App\Http\Controllers;

use App\Models\Message;
use App\Models\Patient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Barryvdh\DomPDF\Facade\Pdf;

class PatientController extends Controller
{
    public function newPatient(Request $request){
        $patient = new Patient();
        $patient->nameOfArabic = $request->nameOfArabic;
        $patient->nameOfEnglish = $request->nameOfEnglish;
        $patient->gender = $request->gender;
        $patient->dob = $request->dob;
        $patient->nationalNumber = $request->nationalNumber;
        $patient->nationality = $request->nationality;
        $patient->IdPatient = $request->IdPatient;
        $patient->passwordPatient = $request->passwordPatient;
        $patient->testName = $request->testName;
        $patient->testResult = $request->testResult;
        $patient->collectionDate = $request->collectionDate;
        $patient->reportingDate = $request->reportingDate;
        $patient->Unit = $request->Unit;
        $patient->brunch = $request->brunch;
        $patient->range = $request->range;
        $patient->report = $request->report;
        $patient->imagePatient = $request->imagePatient;

        $message = $this->isValidateInfo($patient->toArray());
        if($message->passes()){
            $patient->save();
            return response()->json(['status'=>' Successful booking']);
        }else{
            return response()->json(['error'=>$message->errors()]);

        }
    }
    protected function isValidateInfo(array $request){

        $message = Validator::make($request, [
            'nameOfArabic'        => 'required',
            'nameOfEnglish'       => 'required',
            'gender'              => 'nullable|boolean',
            'nationalNumber'      => 'nullable|numeric',
        ]);

        if (isset($request['attach'])) {
            Validator::make($request,[
                'attach' => 'mimes:jpg,pdf |max:4096',
            ],$message[] = [
                'mimes' => 'Please insert image only',
                'max'   => 'Image should be less than 4 MB'
            ]);
        }
    }
    protected function makePDF($khatem = 1, $sheaar =1 ){

    }
}
