<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function home(){
        return view('front.index');
    }

    public function testResult(){
        return view('front.index');
    }

    public function branches(){
        return view('front.index');
    }
    public function guides(){
        return view('front.index');
    }
    public function about(){
        return view('front.index');
    }
    public function contact(){
        return view('front.contact');
    }
}
