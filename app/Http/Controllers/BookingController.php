<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Message;
use App\Models\Patient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BookingController extends Controller
{

    public function houseVisit(){
        return view('front.index');
    }
    public function SendMessage(Request $request){

        $Item = new Booking();
        $Item->name = $request->name;
        $Item->phoneNumber = $request->phoneNumber;
        $Item->note = $request->note;
        $message = $this->isValidate($Item->toArray());
        if($message->passes()){
            return  $this->saveMessage($Item->toArray());
        }else{
            return response()->json(['error'=>$message->errors()]);
        }
    }
    public function houseVisitStore(Request $request): \Illuminate\Http\JsonResponse
    {

        $Item = new Patient();
        $Item->name = $request->name;
        $Item->phoneNumber = $request->phoneNumber;
        $Item->address = $request->address;
        $Item->note = $request->note;
        $Item->attach = $request->file('attach');
        $message = $this->isValidate($Item->toArray());
        if($message->passes()){
            return  $this->saveBook($Item->toArray());
        }else{
            return response()->json(['error'=>$message->errors()]);

        }
    }
    private function saveBook(array $request): \Illuminate\Http\JsonResponse
    {
        Patient::create(
            [
                'name'          => $request['name'],
                'phoneNumber'   => $request['phoneNumber'],
                'address'       => $request['address'],
                'note'          => $request['note'],
                'file_path'     =>(!$request['attach']) ? null :  $this->upload_file($request['attach']),
                'ipAddress'     =>$this->getUserIpAddr(),
            ]
        );
        return response()->json(['status'=>' Successful booking']);
    }

    private function saveMessage(array $request): \Illuminate\Http\JsonResponse
    {
        Message::create(
            [
                'name'          => $request['name'],
                'phoneNumber'   => $request['phoneNumber'],
                'note'          => $request['note'],
            ]
        );
        return response()->json(['status'=>' Successful booking']);
    }

    function getUserIpAddr(): string
    {
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR']??'localhost';
        }
        return $ip;
    }

    private function upload_file($image_file): string
    {
        $new_name = time() . '_'.$image_file->getClientOriginalName();
        $image_file->move('patient/', $new_name);
        $new_name = 'patient/' . $new_name;
        return $new_name;
    }

    private function isValidate(array $request)
    {
        $message = Validator::make($request, [
            'name'              => 'required',
            'phoneNumber'       => 'required',
        ]);
        if (isset($request['attach'])) {
            Validator::make($request,[
                'attach' => 'mimes:jpg,pdf |max:4096',
            ],$messages = [
                'mimes' => 'Please insert image only',
                'max'   => 'Image should be less than 4 MB'
            ]);
        }
        return $message;

    }
}
