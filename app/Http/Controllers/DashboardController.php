<?php

namespace App\Http\Controllers;

use App\Models\Patient;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    // Preview All Request
    public function patients(){
        $Items = Patient::get();
        return view('dashboard.patients.index',compact('Items'));
    }
}
