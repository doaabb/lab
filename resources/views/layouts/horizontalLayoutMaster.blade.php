@isset($pageConfigs)
{!! Helper::updatePageConfig($pageConfigs) !!}
@endisset

        <!DOCTYPE html>
{{-- {!! Helper::applClasses() !!} --}}
@php
  $configData = Helper::applClasses();
$configData['mainLayoutType'] === 'horizontal';
@endphp

<html lang="@if(session()->has('locale')){{session()->get('locale')}}@else{{$configData['defaultLanguage']}}@endif" data-textdirection="{{ env('MIX_CONTENT_DIRECTION') === 'rtl' ? 'rtl' : 'ltr' }}" class="{{ ($configData['theme'] === 'light') ? '' : $configData['layoutTheme'] }}">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

  <title>@yield('title') Pharmacy</title>
  <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/logo/favicon.ico')}}">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />

  {{-- Include core + vendor Styles --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/vendors.min.css')) }}" />
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/ui/prism.min.css')) }}" />
  {{-- Vendor Styles --}}
  @yield('vendor-style')
  {{-- Theme Styles --}}

  <link rel="stylesheet" href="{{ asset(mix('css/core.css')) }}" />

  {{-- {!! Helper::applClasses() !!} --}}

  {{-- Page Styles --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/core/menu/menu-types/horizontal-menu.css')) }}" />
  <link rel="stylesheet" href="{{ asset(mix('css/base/core/menu/menu-types/vertical-menu.css')) }}" />
<!-- <link rel="stylesheet" href="{{ asset(mix('css/base/core/colors/palette-gradient.css')) }}"> -->

  {{-- Page Styles --}}
  @yield('page-style')

  {{-- Laravel Style --}}
  <link rel="stylesheet" href="{{ asset(mix('css/overrides.css')) }}" />

  {{-- Custom RTL Styles --}}

  @if($configData['direction'] === 'rtl' && isset($configData['direction']))
    <link rel="stylesheet" href="{{ asset(mix('css/custom-rtl.css')) }}" />
    <link rel="stylesheet" href="{{ asset(mix('css/style-rtl.css')) }}" />
  @endif

  {{-- user custom styles --}}
  <link rel="stylesheet" href="{{ asset(mix('css/style.css')) }}" />

</head>


<body class="horizontal-layout horizontal-menu {{$configData['horizontalMenuType']}} {{ $configData['showMenu'] === true ? '' : '1-column' }}
{{ $configData['blankPageClass'] }} {{ $configData['bodyClass'] }}
{{ $configData['footerType'] }}" data-menu="horizontal-menu" data-col="{{ $configData['showMenu'] === true ? '' : '1-column' }}" data-open="hover" data-layout="{{ ($configData['theme'] === 'light') ? '' : $configData['layoutTheme'] }}" style="{{ $configData['bodyStyle'] }}" data-framework="laravel" data-asset-path="{{ asset('/')}}">

<!-- BEGIN: Header-->
{{-- Include Navbar --}}
@include('panels.navbar_horizontal')

{{-- Include Sidebar --}}
@if((isset($configData['showMenu']) && $configData['showMenu'] === true))
  @include('panels.horizontalMenu')
@endif

<!-- BEGIN: Content-->
<div class="app-content content {{ $configData['pageClass'] }}">
  <div class="content-overlay"></div>
  <div class="header-navbar-shadow"></div>
  @if(($configData['contentLayout']!=='default') && isset($configData['contentLayout']))
    <div class="content-area-wrapper {{ $configData['layoutWidth'] === 'boxed' ? 'container p-0' : '' }}">
      <div class="{{ $configData['sidebarPositionClass'] }}">
{{--        <div class="sidebar">--}}
{{--          --}}{{-- Include Sidebar Content --}}
{{--          @yield('content-sidebar')--}}
{{--        </div>--}}
      </div>
      <div class="{{ $configData['contentsidebarClass'] }}">
        <div class="content-wrapper">
          <div class="content-body">
            {{-- Include Page Content --}}
            @yield('content')
          </div>
        </div>
      </div>
    </div>
  @else
    <div class="content-wrapper {{ $configData['layoutWidth'] === 'boxed' ? 'container p-0' : '' }}">
      {{-- Include Breadcrumb --}}
{{--      @if($configData['pageHeader'] == true)--}}
{{--        @include('panels.breadcrumb')--}}
{{--      @endif--}}

      <div class="content-body">

        {{-- Include Page Content --}}
        @yield('content')

      </div>
    </div>
  @endif

</div>
<!-- End: Content-->



<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

{{-- include footer --}}
@include('panels/footer')

{{-- include default scripts --}}
@include('panels/scripts')

<script type="text/javascript">
  $(window).on('load', function() {
    if (feather) {
      feather.replace({
        width: 14
        , height: 14
      });
    }
  })

</script>
</body>

</html>
