
@extends('layouts/contentLayoutMaster')

@section('title', 'Users')

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
@endsection

@section('content')

    <!-- Complex Headers -->
    <section id="complex-header-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header border-bottom">
                        <h4 class="card-title">All User</h4>
                    </div>
                    <div class="card-datatable">
                        <table id="tbl-pieces" class="table table-striped table-bordered dt-responsive nowrap no-footer" cellspacing="0" style="width:100%;">
                            <thead>
                            <tr>
                                <th >id</th>
                                <th >Name</th>
                                <th>Phone</th>
                                <th >Address</th>
                                <th >Description</th>
                                <th >File</th>
                                <th >Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach( $Items as $index=>$Item)
                                {{--                                @dd($Items[3])--}}
                                <tr class="row_{{ $Item->id }}">
                                    <td >
                                        {{ $index+1 }}
                                    </td>
                                    <td >
                                        <div class="d-flex justify-content-left align-items-center">
                                            <div class="avatar-wrapper">
                                                <div class="avatar  bg-light-warning  mr-1">
                                                    @if($Item->file_path == null)
                                                        <span class="avatar-content">{{ substr($Item->name,0,2) }}</span>
                                                    @else
                                                        <img src="{{ $Item->file_path }}" alt="Avatar" height="32" width="32">
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="d-flex flex-column">
                                                <a href="#" class="user_name text-truncate">
                                                    <span class="font-weight-bold">{{ $Item->name }}</span>
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                    <td >{{ $Item->phoneNumber ??'' }} </td>
                                    <td >
                                            {{ $Item->address }}
                                    </td>
                                    <td >
                                            {{ $Item->note }}
                                    </td>
                                    <td ><img src="{{ $Item->file_path }}" width="50"> </td>
                                    <td>

                                        <a href="#" data-key="{{ $Item->id }}"
                                           class="badge badge-light-danger delete-record">
                                            <i class="fa fa-trash"></i>
                                        </a>
{{--                                        <a href="{{ route('edit_user',['id'=>$Item->id]) }}" data-key="{{ $Item->id }}"--}}
{{--                                           class="badge badge-light-info">--}}
{{--                                            <i class="fa fa-edit" ></i>--}}
{{--                                        </a>--}}
                                        
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/ Complex Headers -->


@endsection


@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
@endsection
@section('page-script')
    {{-- Page js files --}}
    <script>
        var dt_complex = $('#tbl-pieces').DataTable({
            dom:
                '<"d-flex justify-content-between align-items-center mx-0 row"' +
                '<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"' +
                '<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 7,
            lengthMenu: [7, 10, 25, 50, 75, 100],
        });
        $("#list_users").addClass('active');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        {{--$('.delete-record').click(function (e) {--}}
        {{--    e.preventDefault();--}}
        {{--    let id = $(this).data("key");--}}
        {{--    console.log(id);--}}
        {{--    $.ajax({--}}
        {{--        url: '{{ route('sdelete_user') }}',--}}
        {{--        type: 'POST',--}}
        {{--        async: true,--}}
        {{--        cache: false,--}}
        {{--        data: {--}}
        {{--            'id':id--}}
        {{--        },--}}
        {{--        success: function (response) {--}}
        {{--            toastr.success(" Your work has been saved", "Success");--}}
        {{--            $('.row_'+id).hide();--}}

        {{--        },--}}
        {{--        error: function () {--}}
        {{--            toastr.error(" Your work has been not saved", "Error");--}}
        {{--        },--}}
        {{--    })--}}
        {{--});--}}
    </script>
@endsection
