@foreach($product as $pro)
    @if (\request()->get('lang'))
        @if($pro->translate_product)
            <div class="card ecommerce-card">
                <div class="item-img text-center">
                    <a href="{{ route('shop-details',['id'=>$pro->id]) }}@if (\request()->get('lang'))/?lang={{\request()->get('lang')}} @endif">
                        <img class="img-fluid card-img-top"
                             src="{{ $pro->image_path }}" alt="img-placeholder" /></a>
                </div>
                <div class="card-body">
                    <div class="item-wrapper">
                        <div class="item-rating">
                            <ul class="unstyled-list list-inline">

                                @for($i=1 ; $i<=5; $i++)
                                    @if($pro->rate && count($pro->rate)!=0)
                                        @if ((int)($pro->rate->sum('rating')/count($pro->rate)) >= $i)
                                            <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                        @else
                                            <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                        @endif
                                    @else
                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                    @endif
                                @endfor </ul>
                        </div>
                        <div>
                            <h6 class="item-price">${{ $pro->price_after }}</h6>
                        </div>
                    </div>
                    <h6 class="item-name">
                        <a class="text-body" href="{{ route('shop-details',['id'=>$pro->id]) }}@if (\request()->get('lang'))/?lang={{\request()->get('lang')}} @endif">
                            @if (\request()->get('lang'))
                                {{ $pro->translate_product?$pro->translate_product->title:'' }}
                            @else
                                {{ $pro->title }}
                            @endif
                        </a>
                        <span class="card-text item-company">By <a href="javascript:void(0)" class="company-name">
                                             @foreach($pro->component($pro->brand) as $brand)
                                    {{ $brand->name }},
                                @endforeach
                                        </a></span>
                    </h6>
                    <p class="card-text item-description">

                        {{ $pro->translate_product?$pro->translate_product->description:'هذا المنتج غير متوفر في لغتك' }}</p>
                </div>
                <div class="item-options text-center">
                    <div class="item-wrapper">
                        <div class="item-cost">
                            <h4 class="item-price">${{ $pro->price_after }}</h4>
                        </div>
                    </div>
                    {{--                                <a href="javascript:void(0)" class="btn btn-light btn-wishlist">--}}
                    {{--                                    <i data-feather="heart"></i>--}}
                    {{--                                    <span>Wishlist</span>--}}
                    {{--                                </a>--}}
                    <a href="javascript:void(0)" class="btn btn-primary btn-cart">
                        <i data-feather="shopping-cart"></i>
                        <span class="add-to-cart">Add to cart</span>
                    </a>
                </div>
            </div>

        @endif
    @else

        <div class="card ecommerce-card">
            <div class="item-img text-center">
                <a href="{{ route('shop-details',['id'=>$pro->id]) }}@if (\request()->get('lang'))/?lang={{\request()->get('lang')}} @endif">
                    <img class="img-fluid card-img-top"
                         src="{{ $pro->image_path }}" alt="img-placeholder" /></a>
            </div>
            <div class="card-body">
                <div class="item-wrapper">
                    <div class="item-rating">
                        <ul class="unstyled-list list-inline">

                            @for($i=1 ; $i<=5; $i++)
                                @if($pro->rate && count($pro->rate)!=0)
                                    @if ((int)($pro->rate->sum('rating')/count($pro->rate)) >= $i)
                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                    @else
                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                    @endif
                                @else
                                    <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                @endif
                            @endfor </ul>
                    </div>
                    <div>
                        <h6 class="item-price">${{ $pro->price_after }}</h6>
                    </div>
                </div>
                <h6 class="item-name">
                    <a class="text-body" href="{{ route('shop-details',['id'=>$pro->id]) }}@if (\request()->get('lang'))/?lang={{\request()->get('lang')}} @endif">
                        {{ $pro->title }}</a>
                    <span class="card-text item-company">By <a href="javascript:void(0)" class="company-name">
                                             @foreach($pro->component($pro->brand) as $brand)
                                {{ $brand->name }},
                            @endforeach
                                        </a></span>
                </h6>
                <p class="card-text item-description">

                    {{ $pro->description }}</p>
            </div>
            <div class="item-options text-center">
                <div class="item-wrapper">
                    <div class="item-cost">
                        <h4 class="item-price">
                            ${{ $pro->price_after }}</h4>
                    </div>
                </div>
                {{--                                <a href="javascript:void(0)" class="btn btn-light btn-wishlist">--}}
                {{--                                    <i data-feather="heart"></i>--}}
                {{--                                    <span>Wishlist</span>--}}
                {{--                                </a>--}}
                <a href="javascript:void(0)" data-id="{{ $pro->id}}" class="btn btn-primary btn-cart add_to_cart">
                    <i data-feather="shopping-cart"></i>
                    <span class="add-to-cart ">Add to cart</span>
                </a>
            </div>
        </div>
    @endif
@endforeach

{!! $product->links() !!}
