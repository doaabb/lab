
<ul class="dropdown-menu dropdown-menu-media dropdown-menu-right" id="header_shop_cart">
        <li class="dropdown-menu-header">
            <div class="dropdown-header d-flex">
                <h4 class="notification-title mb-0 mr-auto">My Cart</h4>
                <div class="badge badge-pill badge-light-primary">{{ session('item') ?? 0 }} Items</div>
            </div>
        </li>

        <li class="scrollable-container media-list">
            @if(session('cart'))
                @foreach(session('cart') as $id => $details)
            <div class="media align-items-center">
                <img class="d-block rounded mr-1" src="{{ $details['photo'] }}" alt="donuts" width="62">
                <div class="media-body"><i class="ficon cart-item-remove" data-feather="x"></i>
                    <div class="media-heading">
                        <h6 class="cart-item-title">
                            <a class="text-body"
                               href="{{ route('shop-details',['id'=>$id]) }}">
                                {{ $details['name'] }}</a></h6>
{{--                        <small class="cart-item-by">x{{ $details['quantity'] }}</small>--}}
                    </div>
                    <div class="cart-item-qty">
                        X {{ $details['quantity'] }}
{{--                        <div class="input-group">--}}
{{--                            <input class="touchspin-cart" type="number" value="{{ $details['quantity'] }}">--}}
{{--                        </div>--}}
                    </div>
                    <h5 class="cart-item-price">${{ $details['price'] }}</h5>
                </div>
            </div>
                @endforeach
            @endif
        </li>

        <li class="dropdown-menu-footer">
            <div class="d-flex justify-content-between mb-1">
                <h6 class="font-weight-bolder mb-0">Total:</h6>
                <h6 class="text-primary font-weight-bolder mb-0">${{ session('total') ?? 0 }}</h6>
            </div><a class="btn btn-primary btn-block" href="{{url('app/ecommerce/checkout')}}">Checkout</a>
        </li>
    </ul>


