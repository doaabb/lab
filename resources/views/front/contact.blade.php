@extends('layouts/horizontalLayoutMaster')

@section('title', 'Pharmacy')

@section('vendor-style')
    <!-- Vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/nouislider.min.css')) }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    {{--    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">--}}
@endsection
@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-sliders.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-ecommerce.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
    <style>
        .card-img, .card-img-top {
            border-top-left-radius: calc(0.428rem - 1px);
            border-top-right-radius: calc(0.428rem - 1px);
            width: 410px;
        }

        .horizontal-layout.navbar-floating:not(.blank-page) .app-content {
            padding: calc(2rem + 4.45rem + 1.3rem) 2rem 0;
        }
    </style>
@endsection

@section('content')
    <div class="container app-content ">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Send Message') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if($errors)
                            @foreach ($errors->all() as $message)
                                <div class="alert alert-danger" role="alert">
                                    {{ $message }}
                                </div>
                            @endforeach
                        @endif
                        <form class="form-horizontal"   enctype="multipart/form-data">
                        {{ csrf_field()}}
                        <!-- Form Name -->
                            <h6 class="m-t-10">Enter Your Message</h6><hr>

                            <!-- Appended Input-->
                            <!-- name input-->
                            <div class="form-group row">
                                <label class="col-lg-12 control-label text-lg-left" for="name">Your Name</label>
                                <div class="col-lg-12">
                                    <input id="name" name="name" type="text" required placeholder="Enter your name" class="form-control btn-square input-md">
                                    <p class="help-block">Required </p>
                                </div>
                            </div>


                            <!-- Phone input-->
                            <div class="form-group row">
                                <label class="col-lg-12 control-label text-lg-left" for="phoneNumber">Phone Number</label>
                                <div class="col-lg-12">
                                    <input id="phoneNumber" name="phoneNumber" type="text" required placeholder="Phone Number " class="form-control btn-square input-md">
                                    <p class="help-block">Required </p>
                                </div>
                            </div>

                            <!-- Phone input-->
                            <div class="form-group row">
                                <label class="col-lg-12 control-label text-lg-left" for="phoneNumber">Description</label>
                                <div class="col-lg-12">
                                    <textarea id="phoneNumber" name="note" type="text"
                                              required  class="form-control btn-square input-md">

                                    </textarea>
                                    <p class="help-block">Required </p>
                                </div>
                            </div>

                            <!-- Button -->
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <input type="submit" id="submit" name="submit" class="btn btn-primary">
                                </div>
                            </div>

                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>


@endsection


@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/pages/app-ecommerce.js')) }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("form").on("submit", function(event){
            event.preventDefault();

            let formData = new FormData(this);
            var formValues= $(this).serialize();
            $.ajax({
                url: '{{ route('SendMessage') }}',
                type: 'POST',
                async: true,
                cache: false,
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    printMsg(response);
                },
                error: function (msg) {
                    $.each( msg.error, function( key, value ) {
                        toastr.error(value, "Danger");
                    });
                },
            })
        });
        function printMsg (msg) {
            if($.isEmptyObject(msg.error)){
                console.log(msg.success);
                toastr.success(" Your work has been saved", "Success");
                $('input').val('') ;
            }else {
                $.each(msg.error, function (key, value) {
                    toastr.error(value, "Danger");
                });
            }
        }
    </script>

@endsection

