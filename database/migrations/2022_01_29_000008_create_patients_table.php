<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->id();
            $table->string('nameOfArabic');
            $table->string('nameOfEnglish');
            $table->boolean('gender')->comment('0 if male , 1 if female')->default(0)->nullable();
            $table->date('dob')->nullable();
            $table->string('nationalNumber')->nullable();
            $table->string('nationality')->nullable();
            $table->string('IdPatient')->nullable();
            $table->string('passwordPatient')->nullable();
            $table->string('testName')->nullable();
            $table->string('testResult')->nullable();
            $table->dateTime('collectionDate')->nullable();
            $table->dateTime('reportingDate')->nullable();
            $table->string('Unit')->nullable();
            $table->string('brunch')->nullable();
            $table->string('range')->comment('Positive Or Negative')->nullable();
            $table->string('report')->nullable();
            $table->string('imagePatient')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
