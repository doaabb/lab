<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class LaratrustSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'super_admin',
            'display_name' => 'Super admin',
            'description' => 'Can do anything in the project',
        ]);
        // user role
         Role::create([
            'name' => 'user',
            'display_name' => 'User',
            'description' => 'Can do specific tasks ',
        ]);
        // doctor role
        Role::create([
            'name' => 'doctor',
            'display_name' => 'Doctor',
            'description' => 'Can do specific tasks ',
        ]);
        // pharmacy role
        Role::create([
            'name' => 'pharmacy',
            'display_name' => 'Pharmacy',
            'description' => 'Can do specific tasks ',
        ]);

    }
}
