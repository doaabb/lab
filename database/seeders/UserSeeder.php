<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $user = User::create([
            'name' => 'super admin',
            'email' => 'super_admin@app.com',
            'password' => Hash::make('12345678'),
        ]);
        $user->attachRole('super_admin');

        $user = User::create([
            'name' => 'doctor',
            'email' => 'doctor@app.com',
            'password' => Hash::make('12345678'),
        ]);
        $user->attachRole('doctor');

        $user = User::create([
            'name' => 'user',
            'email' => 'user@app.com',
            'password' => Hash::make('12345678'),
        ]);
        $user->attachRole('user');

        $user = User::create([
            'name' => 'pharmacy',
            'email' => 'pharmacy@app.com',
            'password' => Hash::make('12345678'),
        ]);
        $user->attachRole('pharmacy');

    }
}
